# -*- coding: utf-8 -*-
"""
Created on Mon May  8 13:07:01 2023

@author: wiesbrock
"""
import pandas as pd
import numpy as np
import matplotlib.pylab as plt
import seaborn as sns
from scipy.stats import zscore
import numpy as np
from scipy.signal import argrelextrema
import math
import os
import glob

def distance(x2,x1,y2,y1):
    dist = np.sqrt( (x2 - x1)**2 + (y2 - y1)**2 )
    return dist

def punkt_in_viereck(punkt, eckpunkte):
    x, y = punkt

    # Überprüfen, ob der Punkt links oder rechts von der Fläche liegt
    if x < min(eckpunkte[0][0], eckpunkte[1][0], eckpunkte[2][0], eckpunkte[3][0]) or \
       x > max(eckpunkte[0][0], eckpunkte[1][0], eckpunkte[2][0], eckpunkte[3][0]):
        return False

    # Überprüfen, ob der Punkt über oder unter der Fläche liegt
    if y < min(eckpunkte[0][1], eckpunkte[1][1], eckpunkte[2][1], eckpunkte[3][1]) or \
       y > max(eckpunkte[0][1], eckpunkte[1][1], eckpunkte[2][1], eckpunkte[3][1]):
        return False

    # Überprüfen, ob der Punkt innerhalb der Fläche liegt
    # Verwendung der "Point-in-Polygon"-Methode
    n = len(eckpunkte)
    inside = False
    p1x, p1y = eckpunkte[0]
    for i in range(n + 1):
        p2x, p2y = eckpunkte[i % n]
        if y > min(p1y, p2y):
            if y <= max(p1y, p2y):
                if x <= max(p1x, p2x):
                    if p1y != p2y:
                        xinters = (y - p1y) * (p2x - p1x) / (p2y - p1y) + p1x
                    if p1x == p2x or x <= xinters:
                        inside = not inside
        p1x, p1y = p2x, p2y

    return inside

path=r'C:\Users\wiesbrock\Desktop\cage_edge\*.csv'
file_list=glob.glob(path)

ratio_list=[]
name_list=[]
date_list=[]

for i in range(len(file_list)):

    

    data=pd.read_csv(file_list[i],delimiter=',',skiprows=1)

    topleft_x=data["topleft"]
    topleft_y=data["topleft.1"]
    likelihood_topleft=data["topleft.2"]

    topright_x=data["topright"]
    topright_y=data["topright.1"]
    likelihood_topright=data["topright.2"]

    bottomright_x=data["bottomright"]
    bottomright_y=data["bottomright.1"]
    likelihood_bottomright=data["bottomright.2"]

    bottomleft_x=data["bottomleft"]
    bottomleft_y=data["bottomleft.1"]
    likelihood_bottomleft=data["bottomleft.2"]

    snout_x=data["snout"]
    snout_y=data["snout.1"]
    likelihood_snout=data["snout.2"]



    snout_x=snout_x[1:].astype(float)
    snout_y=snout_y[1:].astype(float)*-1
    likelihood_snout=likelihood_snout[1:].astype(float)

    topleft_x=topleft_x[1:].astype(float)
    topleft_y=topleft_y[1:].astype(float)*-1
    likelihood_topleft=likelihood_topleft[1:].astype(float)

    topleft_x=topleft_x[likelihood_topleft>0.9999]
    topleft_y=topleft_y[likelihood_topleft>0.9999]

    topleft_x=np.array(topleft_x)[0]
    topleft_y=np.array(topleft_y)[0]


    topright_x=topright_x[1:].astype(float)
    topright_y=topright_y[1:].astype(float)*-1
    likelihood_topright=likelihood_topright[1:].astype(float)

    topright_x=topright_x[likelihood_topright>0.9999]
    topright_y=topright_y[likelihood_topright>0.9999]

    topright_x=np.array(topright_x)[0]
    topright_y=np.array(topright_y)[0]

    bottomright_x=bottomright_x[1:].astype(float)
    bottomright_y=bottomright_y[1:].astype(float)*-1
    likelihood_bottomright=likelihood_bottomright[1:].astype(float)

    bottomright_x=bottomright_x[likelihood_bottomright>0.9999]
    bottomright_y=bottomright_y[likelihood_bottomright>0.9999]

    bottomright_x=np.array(bottomright_x)[0]
    bottomright_y=np.array(bottomright_y)[0]

    bottomleft_x=bottomleft_x[1:].astype(float)
    bottomleft_y=bottomleft_y[1:].astype(float)*-1
    likelihood_bottomleft=likelihood_bottomleft[1:].astype(float)

    bottomleft_x=bottomleft_x[likelihood_bottomleft>0.9999]
    bottomleft_y=bottomleft_y[likelihood_bottomleft>0.9999]

    bottomleft_x=np.array(bottomleft_x)[0]
    bottomleft_y=np.array(bottomleft_y)[0]

    plt.plot(snout_x,snout_y,'k-')

    plt.plot(topleft_x,topleft_y,'wo')
    plt.plot(topright_x,topright_y,'wo')
    plt.plot(bottomleft_x,bottomleft_y,'wo')
    plt.plot(bottomright_x,bottomright_y,'wo')

    dist_topleft_topright=distance(topleft_x,topright_x,topleft_y,topright_y)
    edge_topleft_topright=dist_topleft_topright*0.1
    edge_topleft_x=topleft_x+edge_topleft_topright
    edge_topright_x=topright_x-edge_topleft_topright

    dist_bottomleft_bottomright=distance(bottomleft_x,bottomright_x,bottomleft_y,bottomright_y)
    edge_bottomleft_bottomright=dist_bottomleft_bottomright*0.1
    edge_bottomleft_x=bottomleft_x+edge_bottomleft_bottomright
    edge_bottomright_x=bottomright_x-edge_bottomleft_bottomright

    dist_bottomleft_topleft=distance(bottomleft_x,topleft_x,bottomleft_y,topleft_y)
    edge_bottomleft_topleft=dist_bottomleft_topleft*0.1
    edge_bottomleft_y=bottomleft_y+edge_bottomleft_topleft
    edge_topleft_y=topleft_y-edge_bottomleft_topleft

    dist_bottomright_topright=distance(bottomright_x,topright_x,bottomright_y,topright_y)
    edge_bottomright_topright=dist_bottomright_topright*0.1
    edge_bottomright_y=bottomright_y+edge_bottomright_topright
    edge_topright_y=topright_y-edge_bottomright_topright



    plt.plot(edge_topleft_x,edge_topleft_y,'g.')
    plt.plot(edge_topright_x,edge_topright_y,'g.')
    plt.plot(edge_bottomleft_x,edge_bottomleft_y,'g.')
    plt.plot(edge_bottomright_x,edge_bottomright_y,'g.')


    edge_all=np.zeros((len(snout_x)))

    eckpunkte=[(edge_topleft_x,edge_topleft_y),(edge_topright_x,edge_topright_y),(edge_bottomright_x,edge_bottomleft_y),(edge_bottomleft_x,edge_bottomright_y)]
    snout_x=np.array(snout_x)
    snout_y=np.array(snout_y)
    for i in range(len(snout_x)):
        punkt=(snout_x[i],snout_y[i])
        edge_all[i] = punkt_in_viereck(punkt, eckpunkte)
        #print(ergebnis)  # True
    
    plt.plot(snout_x[edge_all==False],snout_y[edge_all==False],'g.')

    ratio_outside=np.sum(edge_all)
    ratio_list.append(ratio_outside)

name_list=np.array(name_list)
date_list=np.array(date_list)
ratio_list=np.array(ratio_list)

df = pd.DataFrame({'mouse_id': name_list,
                   'date': date_list,
                   'edge': ratio_list})

# Schreiben des Datenrahmens in eine Excel-Datei
df.to_excel('readout.xlsx', index=False)







