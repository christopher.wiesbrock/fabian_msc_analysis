# -*- coding: utf-8 -*-
"""
Created on Sun Apr 23 18:46:33 2023

@author: wiesbrock
"""
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 26 10:46:17 2023

@author: wiesbrock
"""

#Zeilen 36, 78 und 79 müssen angepasst werden

import numpy as np
import matplotlib.pylab as plt
import seaborn as sns
import scipy.stats as stats
import glob
from scipy.signal import argrelextrema
import pandas as pd
import os

def distance(x2,x1,y2,y1):
    dist = np.sqrt( (x2 - x1)**2 + (y2 - y1)**2 )
    return dist

name_list=[]
immobile_list=[]
median_speed_list=[]
percent_left_list=[]
date_list=[]


path_folder=r'C:\Users\wiesbrock\Desktop\Auswertung\DLC csv single animal\*.csv'
file_list=glob.glob(path_folder)
for i in file_list:
    #Pfad und Daten werden eingelesen
    path=os.path.abspath(i)
    #mouse id aus dem Dateinamen
    mouse_name=i[-80:-75]
    date=i[-74:-68]

    data=pd.read_csv(path,delimiter=',',header=None)

    


    #Werte für die Position der Nase werden in Variablen überführt
    snout_x=data[10]
    snout_y=data[11]
    snout_likelihood=data[12]

    #Variablen werden zu Arrays umgewandelt, damit diese von matplotlib leichter zu verarbeiten sind
    snout_x=np.array(snout_x)
    snout_y=np.array(snout_y)


    #Die ersten vier Reihen werden rausgenommen, da diese Infos enthalten, die wir nicht mehr brauchen

    snout_x=snout_x[4:].astype(float)
    snout_y=snout_y[4:].astype(float)

    #Filter anhand der Likelihood um grobes Misstracking rauszufiltern
    snout_likelihood=snout_likelihood[4:].astype(float)
    snout_x[snout_likelihood<0.95]=np.nan
    snout_y[snout_likelihood<0.95]=np.nan


    distance_values=np.zeros((len(snout_x),1))
    for i in range(len(snout_x)-1):
        distance_values[i]=distance(snout_x[i+1],snout_x[i],snout_y[i+1],snout_y[i])



    #40 cm = 345 pixel
    pixel_per_cm = 8.625
    fps=20
    #1 Value = 1/20 sec

    for i in range(len(distance_values)):
        distance_values[i]=(distance_values[i]*fps)/pixel_per_cm
        
    distance_values=distance_values[distance_values<50]
    hist,edges=np.histogram(distance_values)
    sns.distplot(distance_values)
    sns.despine()
    plt.ylabel('Speed [cm/s]')
    plt.figure()
    plt.plot(snout_x,snout_y)
    sns.despine()
    plt.xlabel('x_position [pixel]')
    plt.ylabel('y_position [pixel]')
    
    plt.figure()
    
    plt.plot(np.cumsum(distance_values))
    sns.despine()
    
    #middle line

    x_mid=(np.nanmax(snout_x)+np.nanmin(snout_x))/2
    y_mid=(np.nanmax(snout_y)+np.nanmin(snout_y))/2
    
    percent_left=(len(snout_x[snout_x<x_mid])/len(snout_x))*100
    
    #immobile
    hist,edges=np.histogram(distance_values)
    immobile=(hist[0]/np.sum(hist))*100
    
    name_list.append(mouse_name)
    immobile_list.append(immobile)
    median_speed_list.append(np.median(distance_values))
    percent_left_list.append(percent_left)
    date_list.append(date)
    
name_list=np.array(name_list)
immobile_list=np.array(immobile_list)
median_speed_list=np.array(median_speed_list)
percent_left_list=np.array(percent_left_list)
date_list=np.array(date_list)

df = pd.DataFrame({'mouse_id': name_list,
                   'date': date_list,
                   'immobile': immobile_list,
                   'median speed': median_speed_list,
                   'percent_left': percent_left_list})

# Schreiben des Datenrahmens in eine Excel-Datei
df.to_excel('readout.xlsx', index=False)
    



