# -*- coding: utf-8 -*-
"""
Created on Wed May  3 08:05:45 2023

@author: wiesbrock
"""

import h5py
import numpy as np
import glob
from tqdm import tqdm
import csv

path=r"Y:/File transfer/Christopher_transfer/von Fabian/H5 zu csv/"
search=path+'*'
files=glob.glob(search)

for i in tqdm(range(len(files))):
    file=files[i]
    read=h5py.File(file, 'r')
    keys_1=list(read.keys())
    
    data1=read[keys_1[0]]
    
    new_keys=list(data1.keys())
    
    p_values=data1['P']
    names=data1['class_names']
    
    
    decoded_names = [name.decode('utf-8') for name in names]

    
    arr = p_values

    # Suchen Sie den höchsten Wert in jeder Zeile und speichern Sie die Spaltenindizes
    max_indices = np.argmax(arr, axis=1)

    # Erstellen Sie ein Array mit Nullen und setzen Sie den höchsten Wert auf 1
    result = np.zeros_like(arr)
    result[np.arange(len(max_indices)), max_indices] = 1
    arr=result
    
    with open(file[:-2]+'.csv', mode='w', newline='') as file:

        # Definieren Sie den Schreibkopf
        header = decoded_names

        # Initialisieren Sie den CSV-Schreibprozess
        writer = csv.writer(file, delimiter=',')

        # Schreiben Sie den Header in die CSV-Datei
        writer.writerow(header)

        # Schreiben Sie jede Zeile des Arrays in die CSV-Datei
        for row in arr:
            writer.writerow(row)

    # Ausgabe einer Bestätigungsnachricht
print('Datei wurde erfolgreich gespeichert.')
    
